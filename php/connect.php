<?php
// Conectando y seleccionado la base de datos  
$dbconn = pg_connect("host=186.147.28.75 dbname=webclinical user=oftalmocenter password=oftalmo2017InHouse")
    or die('No se ha podido conectar: ' . pg_last_error());

// Realizando una consulta SQL
$query = 'select * from public.tab_acto_quirurgico as tab_acto_quirurgico 
inner join  public.tab_estado as tab_estado on tab_acto_quirurgico.aqu_est_id = tab_estado.est_id';
$result = pg_query($query) or die('La consulta fallo: ' . pg_last_error());

// Imprimiendo los resultados en HTML
echo "<table>\n";
while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
    echo "\t<tr>\n";
    foreach ($line as $col_value) {
        echo "\t\t<td>$col_value</td>\n";
    }
    echo "\t</tr>\n";
}
echo "</table>\n";

// Liberando el conjunto de resultados
pg_free_result($result);

// Cerrando la conexión
pg_close($dbconn);
?>