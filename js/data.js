const DATA = [
    {
        name: 'tab_acto_quirurgico',
        fullname: 'public.tab_acto_quirurgico',
        columns: ['aqu_id', 'aqu_codigo', 'aqu_nombre', 'aqu_descripcion', 'aqu_est_id', 'aqu_fecha_creacion'],
        relation: ['tab_estado-|-tab_estado.est_id-|-tab_acto_quirurgico.aqu_est_id']
    },
    {
        name: 'tab_anestesia',
        fullname: 'public.tab_anestesia',
        columns: ['ane_id', 'ane_nombre', 'ane_descripcion', 'ane_est_id', 'ane_usuario_creacion', 'ane_fecha_creacion'],
        relation: ['tab_estado-|-tab_estado.est_id-|-tab_anestesia.ane_est_id']
    },
    {
        name: 'tab_estado',
        fullname: 'public.tab_estado',
        columns: ['est_id', 'est_nombre', 'est_descripcion']
    },
    {
        name: '',
        fullname: '',
        columns: [],
        relation: []
    }
];